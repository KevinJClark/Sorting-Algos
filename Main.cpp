#include <cstdlib>
#include <iostream>

int InsertionSort(int Array[], int NumOfValues)
{
	int CompareCount = 0;
	for(int i = 0; i < NumOfValues; i++)
	{ //Loops for each element in the array
		int j = i;
		for(int j = i; j > 0; j--)
		{ //Loops thru sub-array. Finds where the next element should be placed
			if(Array[j-1] > Array[j])
			{
				int Temp = Array[j];
				Array[j] = Array[j-1];
				Array[j-1] = Temp;
			}
			CompareCount++;
		}
	}
	
	return CompareCount;
} //Sorts the array and returns the number of iterations

int MergeSort(int Array[], int Low, int High)
{
	int CompareCount = 0;
	
	if(Low < High) //When Low = High, array segments are size 1
	{
		int Middle = Low + (High - Low) / 2;
		//Recursive calls to MergeSort
		CompareCount += MergeSort(Array, Low, Middle);
		CompareCount += MergeSort(Array, Middle+1, High);
		
		//Segment1Array is the left array, Segment2Array is the right array
		int Segment1Size = Middle - Low + 1;
		int Segment2Size = High - Middle;
		int LeftArray[Segment1Size], RightArray[Segment2Size]; //Make 2 half-size arrays
		int Segment1Index = 0, Segment2Index = 0, MergedIndex = Low; //Indexes for the 3 arrays
	
		//Copy unsorted data into the left and right sub-arrays
		for(int i = 0; i < Segment1Size; i++)
		{
			LeftArray[i] = Array[Low + i];
		}
		for(int j = 0; j < Segment2Size; j++)
		{
			RightArray[j] = Array[Middle+1 + j];
		}
	
		//While neither segments are empty
		while(Segment1Index < Segment1Size && Segment2Index < Segment2Size)
		{
			if(LeftArray[Segment1Index] <= RightArray[Segment2Index])
			{
				Array[MergedIndex] = LeftArray[Segment1Index];
				Segment1Index++;
				MergedIndex++;
			}
			else //(LeftArray[Segment1Index] > RightArray[Segment2Index])
			{
				Array[MergedIndex] = RightArray[Segment2Index];
				Segment2Index++;
				MergedIndex++;
			}
			CompareCount++;
		}
	
		//Copy remaining items in from remaining array segment after one is exausted.
		while(Segment1Index < Segment1Size)
		{
			Array[MergedIndex] = LeftArray[Segment1Index];
			Segment1Index++;
			MergedIndex++;
			CompareCount++;
		}
		while(Segment2Index < Segment2Size)
		{
			Array[MergedIndex] = RightArray[Segment2Index];
			Segment2Index++;
			MergedIndex++;
			CompareCount++;
		}
	}
	
	return CompareCount;
} //Sorts the array and returns the number of iterations

int QuickSort(int Array[], int Low, int High)
{
	int CompareCount = 0;	
	int Temp;
	int LeftIndex = Low, RightIndex = High;
	int Pivot = Array[(Low+High)/2]; //Picking the middle point as pivot
	
	while(LeftIndex <= RightIndex)
	{
		while(Array[LeftIndex] < Pivot)
		{
			LeftIndex++;
			CompareCount++;
		}
		while(Array[RightIndex] > Pivot)
		{
			RightIndex--;
			CompareCount++;
		}
		if(LeftIndex <= RightIndex)
		{
			Temp = Array[LeftIndex]; //Swap items from left and right
			Array[LeftIndex] = Array[RightIndex]; //sections.
			Array[RightIndex] = Temp; //Requires a temp variable
			LeftIndex++;
			RightIndex--;
			CompareCount++;
		}
		
	}
	
	if(Low < RightIndex)
	{
		CompareCount += QuickSort(Array, Low, RightIndex);
	}
	if (High > LeftIndex)
	{
		CompareCount += QuickSort(Array, LeftIndex, High);
	}
	
	return CompareCount;
} //Sorts the array and returns the number of iterations

int main()
{
	int NumOfValues;
	std::cout << "How many values in the array?: ";
	std::cin >> NumOfValues;
	int Seed;
	std::cout << "Random seed?: ";
	std::cin >> Seed;
	srand(Seed);
	
	int RandTemp, InsertStatistic, MergeStatistic, QuickStatistic;
	int InsertArray[NumOfValues]; //4 arrays -- one unsorted, and 3 sorted
	int MergeArray[NumOfValues];
	int QuickArray[NumOfValues];
	int UnsortedArray[NumOfValues];
	char PrintYN;
	
	//Fill up the 3 arrays with the same random values
	for(int i = 0; i < NumOfValues; i++)
	{
		RandTemp = rand() % 500 + 1; //range 1..500
		UnsortedArray[i] = RandTemp;
		InsertArray[i] = RandTemp;
		MergeArray[i] = RandTemp;
		QuickArray[i] = RandTemp;
	}
	
	InsertStatistic = InsertionSort(InsertArray, NumOfValues);	//Function
	MergeStatistic = MergeSort(MergeArray, 0, NumOfValues-1);	//Calls
	QuickStatistic = QuickSort(QuickArray, 0, NumOfValues-1);	//Here
	
	std::cout << "InsertSort Count: " << InsertStatistic << std::endl; //Statistics
	std::cout << "MergeSort Count:  " << MergeStatistic << std::endl;
	std::cout << "QuickSort Count:  " << QuickStatistic << std::endl << std::endl;
	
	std::cout << "Do you want to see the arrays? (y/n): ";
	std::cin >>  PrintYN;
	if(PrintYN == 'y' || PrintYN == 'Y')
	{
		std::cout << "Unsorted array" << std::endl;
		for(int i = 0; i < NumOfValues; i++)
		{
			std::cout << UnsortedArray[i] << " ";
		}
		std::cout << std::endl << "Sorted Insert array" << std::endl;
		for(int i = 0; i < NumOfValues; i++)
		{
			std::cout << InsertArray[i] << " ";
		}
		std::cout << std::endl << "Sorted Merge array" << std::endl;
		for(int i = 0; i < NumOfValues; i++)
		{
			std::cout << MergeArray[i] << " ";
		}
		std::cout << std::endl << "Sorted Quicksort array" << std::endl;
		for(int i = 0; i < NumOfValues; i++)
		{
			std::cout << QuickArray[i] << " ";
		}
		std::cout << std::endl;
	}
	
	return 0;
}
